using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using PirateBorgGenerator;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddRazorPages(opts => 
    opts.Conventions.ConfigureFilter(new IgnoreAntiforgeryTokenAttribute()));
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();
builder.Services.AddLogging();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}
else
{
    app.UseHttpsRedirection();
}

app.UseStaticFiles();
app.UseRouting();
app.MapRazorPages();

app.MapGet("/gen", (HttpRequest req) =>
    {
        var queries = req.Query;
        var name = queries.FirstOrDefault(kvp => kvp.Key == "name").Value.FirstOrDefault() ?? "Nameless";
        var optionalClasses = queries.FirstOrDefault(kvp => kvp.Key.StartsWith("opt")).Value.FirstOrDefault() ?? "false";
        var withOptional = false;
        if (bool.TryParse(optionalClasses, out var choice))
        {
            withOptional = choice;
        }
        
        var character = Generation.generate(name, withOptional);
        return character;
    })
    .WithName("GetNewCharacter")
    .WithOpenApi();

app.Run();