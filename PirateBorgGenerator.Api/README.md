# PirateBorgGenerator.Api - The Web Stuff

This project is actually two things:

1. An API for the `PirateBorgGenerator`
2. A web front-end for the `PirateBorgGenerator` library

The API is currently live at:

[https://pirateborggenerator.bauxite.tech/gen](https://pirateborggenerator.bauxite.tech/gen)

The front-end is live at:

[https://https://pirateborggenerator.bauxite.tech](https://pirateborggenerator.bauxite.tech/)

If you need a lot of pirates. . .[that can be arranged](https://pirateborggenerator.bauxite.tech/MakeNPirates).
