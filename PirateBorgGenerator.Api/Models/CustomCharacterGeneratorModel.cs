﻿namespace PirateBorgGenerator.Api.Models;

public class CustomCharacterGeneratorModel
{
    public bool EnableBrute {get; set;} = false;
    public bool EnableRapscallion {get; set;} = false;
    public bool EnableBuccaneer {get; set;} = false;
    public bool EnableSwashbuckler {get; set;} = false;
    public bool EnableZealot {get; set;} = false;
    public bool EnableSorcerer {get; set;} = false;

    public int NumPirates { get; set; } = 1;
}