﻿namespace PirateBorgGenerator.Api.Models;

public class NewCharacterRequest
{
    public string Name { get; set; } = "Nameless";
    public bool Optional { get; set; } = false;
}