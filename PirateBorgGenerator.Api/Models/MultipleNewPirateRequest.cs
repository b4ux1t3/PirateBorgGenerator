﻿namespace PirateBorgGenerator.Api.Models;

public class MultipleNewPirateRequest
{
    public int NumPirates { get; set; } = 0;
    public bool Optional { get; set; } = false;

}