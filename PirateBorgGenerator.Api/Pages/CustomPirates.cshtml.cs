﻿using Microsoft.AspNetCore.Mvc.RazorPages;
using PirateBorgGenerator.Api.Models;

namespace PirateBorgGenerator.Api.Pages;

public class CustomPirates : PageModel
{
    public Finalize.FinalCharacter[] Pirates { get; set; } = Array.Empty<Finalize.FinalCharacter>();


    public void OnPost(CustomCharacterGeneratorModel customCharacterGeneratorModel)
    {
        Pirates = new Finalize.FinalCharacter[Math.Min(customCharacterGeneratorModel.NumPirates, 20)];
        var classes = new List<string>();
        if (customCharacterGeneratorModel.EnableBrute) classes.Add("brute");
        if (customCharacterGeneratorModel.EnableRapscallion) classes.Add("rapscallion");
        if (customCharacterGeneratorModel.EnableBuccaneer) classes.Add("buccaneer");
        if (customCharacterGeneratorModel.EnableSwashbuckler) classes.Add("swashbuckler");
        if (customCharacterGeneratorModel.EnableZealot) classes.Add("zealot");
        if (customCharacterGeneratorModel.EnableSorcerer) classes.Add("sorcerer");
        if (classes.Count < 1)
        {
            RedirectToPage("CustomGenerator");
        }
        else
        {
            Parallel.For(0, Pirates.Length, i =>
            {
                Pirates[i] = Generation.generateFromClassList(classes.ToArray());
            });
        }
    }
}