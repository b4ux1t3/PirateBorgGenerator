using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using PirateBorgGenerator.Api.Models;

namespace PirateBorgGenerator.Api.Pages;

public class IndexModel : PageModel
{
    [BindProperty] public MultipleNewPirateRequest PirateRequest { get; set; } 

    public void OnGet() {
    }
}