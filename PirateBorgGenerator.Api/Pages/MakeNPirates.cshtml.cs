﻿using System.Collections;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using PirateBorgGenerator.Api.Models;

namespace PirateBorgGenerator.Api.Pages;

public class MakeNPirates : PageModel
{
    [BindProperty] public MultipleNewPirateRequest Request { get; set; } 
}