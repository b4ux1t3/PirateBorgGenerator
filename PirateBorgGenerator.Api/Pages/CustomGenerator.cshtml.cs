﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using PirateBorgGenerator.Api.Models;

namespace PirateBorgGenerator.Api.Pages;

public class CustomGenerator : PageModel
{
    [BindProperty] public CustomCharacterGeneratorModel CustomCharacterGeneratorModel { get; set; } = new();
    
}