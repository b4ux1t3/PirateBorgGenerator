﻿using Microsoft.AspNetCore.Http.HttpResults;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using PirateBorgGenerator.Api.Models;

namespace PirateBorgGenerator.Api.Pages;

public class NewCharacter : PageModel
{

    public Finalize.FinalCharacter _character;

    public void OnGet(NewCharacterRequest characterRequest)
    {
        _character = Generation.generate(characterRequest.Name, characterRequest.Optional);;
    }
   
}