﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using PirateBorgGenerator.Api.Models;

namespace PirateBorgGenerator.Api.Pages;

public class MoarPirates : PageModel
{
    public Finalize.FinalCharacter[] Pirates { get; set; } = Array.Empty<Finalize.FinalCharacter>();


    public void OnPost(MultipleNewPirateRequest pirateRequest)
    {
        Pirates = new Finalize.FinalCharacter[Math.Min(pirateRequest.NumPirates, 20)];

        Parallel.For(0, Pirates.Length, i =>
        {
            Pirates[i] = Generation.generate("Nameless", pirateRequest.Optional);
        });
    }
    
}