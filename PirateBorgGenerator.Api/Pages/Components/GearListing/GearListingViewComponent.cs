﻿using Microsoft.AspNetCore.Mvc;

namespace PirateBorgGenerator.Api.Pages.Components.GearListing;

public class GearListingViewComponent : ViewComponent
{
    public async Task<IViewComponentResult> InvokeAsync((string heading, string item) itemTuple) => View(itemTuple);

}