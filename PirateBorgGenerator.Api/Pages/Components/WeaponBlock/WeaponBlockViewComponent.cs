﻿using Microsoft.AspNetCore.Mvc;

namespace PirateBorgGenerator.Api.Pages.Components.WeaponBlock;

public class WeaponBlockViewComponent : ViewComponent
{
    
    
    public async Task<IViewComponentResult> InvokeAsync(string weapon, string description, string dieRoll) => 
        View((weapon, description, dieRoll));
    
}