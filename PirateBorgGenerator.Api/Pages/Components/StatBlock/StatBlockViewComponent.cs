﻿using Microsoft.AspNetCore.Mvc;

namespace PirateBorgGenerator.Api.Pages.Components.StatBlock;

public class StatBlockViewComponent : ViewComponent
{
    public async Task<IViewComponentResult> InvokeAsync(Character.StatBlock statBlock)
    {
        return View(statBlock);
    }
}