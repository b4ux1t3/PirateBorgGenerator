using Microsoft.AspNetCore.Mvc;

namespace PirateBorgGenerator.Api.Pages.Components.CharacterSheet;

public class CharacterSheetViewComponent: ViewComponent
{
    public async Task<IViewComponentResult> InvokeAsync(Finalize.FinalCharacter? character = null)
    {
        character ??= Generation.generate("", true);
        return View(character);
    }
    
}