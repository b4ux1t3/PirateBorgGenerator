﻿open PirateBorgGenerator

[<EntryPoint>]
let main args =
    
    let name, optionalClasses =
        match args with
        | [| n; flag |] ->
            match bool.TryParse(flag) with
            | true, f -> (n, f)
            | false, _ -> (n, false)
        | _ -> ("Nameless", false)
        
    // let generatedCharacter = Generation.generate name optionalClasses
    
    let generatedCharacter = Generation.generateFromClassList [|"zealot"; |]
    
    printfn $"%A{generatedCharacter}"
    0

