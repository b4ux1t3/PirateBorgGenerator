# PirateBorgGenerator.Cli

This is a very simple command-line utility for generating random [Pirate Borg](https://www.limithron.com/pirateborg) characters.

## Usage

Run the following command, passing the name of your character and, optionally, whether you want to enable optional classes.

```shell
dotnet run -- name [optional_classes true|false]
```

### Example Usage

```shell
➜ dotnet run -- Ian true
{ name = "Ian"
  hitPoints = 4
  container = "Container Satchel"
  cheapGear = "Lantern"
  fancyGear = "Instrument Horn"
  statBlock = { strength = -1
                agility = 0
                presence = -1
                toughness = -1
                spirit = 2 }
  hauntedSoul = ""
  tallTale = ""
  characterClass = "Sorcerer Clairvoyance" }
```
