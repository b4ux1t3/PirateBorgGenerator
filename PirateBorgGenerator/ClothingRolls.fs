﻿module PirateBorgGenerator.ClothingRolls

open PirateBorgGenerator.Dice
open PirateBorgGenerator.Character
open PirateBorgGenerator.Gear
open PirateBorgGenerator.CharacterClassClothingAdjustments

let rollForClothing character =
    let dieToRoll =
        match character.characterClass with
        | Some characterClass -> getCharacterClassClothingDie characterClass
        | None -> D10 |> Some

    let rollOnClothing dieRoll =
        match dieRoll with
        | 1
        | 2 ->
            { character with
                startingClothes = Rags |> Some }
        | 3
        | 4 ->
            { character with
                startingClothes = CommonClothes |> Some }
        | 5 ->
            { character with
                startingClothes = OldUniform |> Some }
        | 6 ->
            { character with
                startingClothes = FancyClothes |> Some }
        | 7 ->
            { character with
                startingClothes = LeatherArmor |> Some }
        | 8 ->
            { character with
                startingClothes = HideArmor |> Some }
        | 9 ->
            { character with
                startingClothes = ChainShirt |> Some }
        | 10 ->
            { character with
                startingClothes = ConquistadorPlate |> Some }
        | i -> failwith $"%d{i}"

    match dieToRoll with
    | Some die -> die |> rollDie |> rollOnClothing
    | None -> character

let rollForHat character =
    let dieToRoll =
        match character.characterClass with
        | Some characterClass -> getCharacterClassHatDie characterClass
        | None -> D12 |> Some
    
    let rollOnHat dieRoll =
        match dieRoll with
        | 1
        | 2 
        | 3
        | 4 -> character
        | 5 ->
            { character with
                startingHat = Wig |> Some }
        | 6 ->
            { character with
                startingHat = Bandanna |> Some }
        | 7 ->
            { character with
                startingHat = Cavalier |> Some }
        | 8 ->
            { character with
                startingHat = Bicorne |> Some }
        | 9 ->
            { character with
                startingHat = PlainTricorne |> Some }
        | 10 ->
            { character with
                startingHat = FancyTricorne |> Some }
        | 11 ->
            { character with
                startingHat = MetalLinedHat |> Some }
        | 12 ->
            { character with
                startingHat = Morion |> Some }
        | i -> failwith $"%d{i}"

    match dieToRoll with
    | Some die -> die |> rollDie |> rollOnHat
    | None -> character
    
let getClothingDescription clothing =
    match clothing with
    | FancyClothes -> "Tier 0. You look amazing."
    | LeatherArmor
    | HideArmor -> "Tier 1: -d2 damage"
    | ChainShirt -> "Tier2: -d4 damage. DR +2 on agility checks including defense"
    | ConquistadorPlate -> "Tier3: -d6 damage. DR +4 on agility checks, defense is DR +2. You'll most likely drown in water"
    | _ -> "Tier 0"