module PirateBorgGenerator.Generation
open System
open PirateBorgGenerator.StartingGearRolls
open PirateBorgGenerator.StartingWeaponRolls
open PirateBorgGenerator.ClothingRolls
open PirateBorgGenerator.StatRolls
open PirateBorgGenerator.Character
open PirateBorgGenerator.CharacterClassRolls
open PirateBorgGenerator.HealthPointsRolls
open PirateBorgGenerator.DevilsLuckRolls
open PirateBorgGenerator.NameRolls
open PirateBorgGenerator.LookupCharacterClass

let generateFromClassList classes =
    
    let character = newCharacter ""
    let classGeneratorOptions =
        classes
        |> Array.filter classStringsContain
        |> Array.map getCharacterClassFromString
        |> Array.filter (fun opt -> match opt with | Some _ -> true | None -> false)
        |> Array.map (fun opt -> match opt with | Some f -> f | None -> failwith "This shouldn't happen")
    let generator = Random.Shared.GetItems(classGeneratorOptions, 1)[0]
    
    character
    |> rollForFirstName
    |> rollForSecondName
    |> rollForLastName
    |> rollStartingContainer
    |> rollCheapGear
    |> rollFancyGear
    |> generator
    |> rollForStats
    |> rollForHp
    |> rollForWeapon
    |> rollForClothing
    |> rollForHat
    |> rollDevilsLuck
    |> Finalize.characterToFinal
    
let generate name optionalClasses =
    let rollForClassWithOption = rollForClass optionalClasses

    name
    |> newCharacter
    |> rollForFirstName
    |> rollForSecondName
    |> rollForLastName
    // container, cheap gear, fancy gear
    |> rollStartingContainer
    |> rollCheapGear
    |> rollFancyGear
    // Roll on class
    |> rollForClassWithOption
    // Roll ability scores
    |> rollForStats
    // Roll on hit points
    |> rollForHp
    // Roll on Weapon,
    |> rollForWeapon 
    // clothing,
    |> rollForClothing
    // hat
    |> rollForHat
    // Roll devil's luck
    |> rollDevilsLuck
    // Roll on tables (we'll get there eventually, maybe)
    |> Finalize.characterToFinal
