﻿module PirateBorgGenerator.SpecialAbilityText

open PirateBorgGenerator.CharacterClasses

let getBruteWeaponText weapon =
    match weapon with
    | BrassAnchor -> "Requires 2 hands, d8 damage, and target's armor is reduced by one tier (-d2) during the attack."
    | WhalingHarpoon -> "d8 damage, can be thrown by testing AGILITY DR10"
    | MeatCleaver -> "Caked in layers of dried blood. d4 damage, and on a damage roll of 1 it spreads disease from one of its prior victims: the target loses d6 HP at the start of its next two turns"
    | PartOfABrokenMast -> "It has a rusty nail protruding from one end. d8 damage, plus an extra d6 on a critical hit"
    | RunicMachete -> "d6 damage. great for chopping down vines and fopdoodles. It glows in the dark if there are undead nearby"
    | RottenCargoNet -> "Test AGILITY DR12 to throw it at something and stop it from moving for d2 rounds. Trapped targets take d2 damage/round"

let getRapscallionAbilityText profession =
    match profession with
    | BackStabber -> "If you attack by surprise, lower the attack DR by 2 and deal d6 extra damage. If taken again, draw a card. Black: lower DR by 2 more. Red: Increase damage die (d6>d8>d10>d12). Joker: Both."
    | Burglar -> "You begin with lock picks. Pickpocket, disarm an enemy or disable a trap: -4 to DR. If taken again: additional -2 to DR."
    | RopeMonkey -> "You're as nimble in the rigging as you are on the deck, If you attack after swinging, jumping, or making an acrobatic maneuver, test AGILITY DR10 to automatically hit and deal +2 damage. If taken again: increase extra damage by +1"
    | SneakyBastard -> "At night or in dark places your AGILITY is +2. You can hide, sneak, and disappear into the shadows: DR16 to spot you"
    | LuckyDevil -> "Whenever you use Devil's Luck, draw a card: 9+: Regain 1 Luck. Joker: Roll on the JOKER TABLE. If taken again: Lower teh card number needed to regain Luck by 1."
    | GrogBrewer -> "Each day you can brew d4 servings of potent grog. You can soak melee weapons in it to use a poison. Gorg in a wound: Test TOUGHNESS DR14 or -d6 HP. If taken again, draw a card: Black: +2 to DR. Red: Increase damage die (d6>d8>d10>d12). Joker: Both."
    
let getBuccaneerFeatureText feature =
    match feature with
    | TreasureHunter -> "Ability tests related to mapping, navigating, treasure hunting, finding & disarming traps, and tracking prey are -3 DR. If taken again: -6 DR."
    | CrackShot -> "All ranged attacks are -2 DR. If taken again: -4 DR"
    | FixBayonets -> "You now have a bayonet (d4). You can attack with it on the same turn you reload. If taken again: d6 damage instead."
    | FocusedAim -> "Attacks against enemies you have already shot at during this combat are -4 DR to hit.. If taken again: the attack also deals d4 extra damage."
    | BuccanCook -> "Months of island life have taught you the art of cooking meats over the buccan fire. You start with d8 rations of exquisite smoked meat. Eating it immediately recovers d4 HP, and you can make d4 more rations from any edible animal you kill. If taken again: recovers d6 instead."
    | Survivalist -> "Your body has developed into a finely tuned machine for existing in the wild. Gain +1 TOUGHNESS. You cannot become infected, sick, or poisoned, and your maximum HP increases by d4. If taken again: -4 to DR and HP increases another d4."

let getSwashbucklerStyleText style =
    match style with
    | OstentatiousFencer -> "Your melee Attack/Defense is DR10 when wielding a rapier or cutlass. When dueling one-on-one, you deal +1 damage."
    | FlintlockFanatic -> "You can attack with up to three pistols on your turn (if  you have them). Reloading one pistol only takes you one round."
    | ScurvyScallywag -> "You don't fight fair. -2 DR when attacking an enemy that has already been attacked this turn."
    | InspiringLeader -> "Once each combat, roll a d4. Each of your allies may add or subtract that value from any one roll during this combat."
    | KnifeKnave -> "You start with 2 knives, and when attacking with them you can make two attacks a turn. They are DR10 to hit, and if the first attack hits, the 2nd is an auto-hit."
    | BlackPowderPoet -> "You start with explosives. Roll d4 times on the Bombs table (pg. 53). Your DR is -2 when throwing bombs."
    
let getZealotPrayerText prayer =
    match prayer with
    | Heal -> "Heal thyself or another for d8 HP."
    | Curse -> "Test Spirit DR10: deal d8 + Spirit damage to an enemy that thou _cannot_ see. DR8 if it has already been hurt this fight."
    | DeathWard -> "Touch the corpse od one who  hath just died and test Spirit DR10: they return to life with 1HP. _Crit_: full HP. _Fumble_: They come back as a zombie and attacketh thee!"
    | ControlWeather -> "Test Spirit DR10 to change teh direction of the wind. If thous succeedeth by 5 or more, thou can also conjure or repel precipitation. _Crit_: Lightning striketh thine enemy, d12. _Fumble_: Lightning strikes thee for d6."
    | BlessedGuidance -> "Thou may add d4 to any roll thee or another player maketh. Use this at any time, including after a roll (does not taketh thy action)."
    | HolyProtection -> "Thou or thine ally gets -4 to DRs to defend for one attack. Use this any time, including after a roll (does not taketh thy action)."
    | DivineLight -> "Bright light radiates from thee for up to d6 x 10 minute. Enemies that see it are -2 DR to defend against."
    | Silence -> "For the next 2d6 x 10 minutes, everything within 25' of thee maketh no sound. The effect only ends when the time doth expire."
    | Sanctuary -> "All thy brethren in sight heal d4 HP."
    | Commune -> "Test Spirit DR8: Asketh thy deity a single \"yay\" or \"nay\" query. Thy response may be \"unclear\" or thou may receiveth no answer."

let getSorcererSpellText spell =
    match spell with
    // 😬 typo from source material.
    | DeadHead -> "You summon a flying, ghostly skull. You may spend your action and test SPIRIT DR12 to have it deal damage to 1 target. It disapates after 1 minute or if it deals any damage. Fumble: it attacks you. It ignores armor and deals: I.2d4 II.2d6 III.2d8"
    | SpiritualPossession -> "One random creature is possessed by a spirit or ghost. Ally: -2 DR to attack and defense. Enemy: -2 DR to attack or defend against it. Any Fumbles related to this creature cause the spirit to leave, stunning the host for 1 round. I.Lasts for d2 rounds II.d4 III.Until combat ends"
    | Protection -> "You summon a ghost or spirit to watch over the souls of you and your allies. Everyone who is protected gets -d2 protection for one hour as if wearing extra armor (does not affect penalties to Strength and Agility, not affected by Fumbles). I.1 soul II.2 souls III.3 souls"
    | Clairvoyance -> "Ask the spirits a question about an adjacent room or area, though their answer may be a lie. Test SPIRIT to know if they are telling the truth. I.DR12 II.DR10 III.DR8"
    | NecroSleep -> "A living creature appears to fall over dead, but when they awake they remember everything. Test SPIRIT DR12 to see if it falls \"dead\" asleep for: I.d2 rounds II.d6 rounds III.d8 hours"
    | RaiseTheDead -> "You can create skeletal thralls from nearby corpses. They are stupid, but obey your verbal commands. They tumble into bones at sunrise. I.1 thrall II.2 thralls III.d2+2 thralls"
    
let getSentientAnimalText animal =
    match animal with
    | CleverMonkey -> "You're an excellent climber."
    | FoulFowl -> "Gain the ability of one random relic and one random ritual. When you are killed, the ghosts of a hundred chickens swarm your assailant, ripping their spiritual soul from their flesh."
    | Jaguar -> "You are a deadly jungle cat."
    | Crocodile -> "You can swim and hide well in water."
    | BilgeRat -> "You are a filthy rodent."
    | LuckyParrot -> "You are a colorful, talking, flying bird."

let getClassAbilityText characterClass =
    match characterClass with
    | Brute weapon -> getBruteWeaponText weapon
    | Rapscallion profession -> getRapscallionAbilityText profession
    | Buccaneer feature -> getBuccaneerFeatureText feature
    | Swashbuckler style -> getSwashbucklerStyleText style
    | Zealot prayer -> getZealotPrayerText prayer
    | Sorcerer spell -> getSorcererSpellText spell
    | SentientAnimal animal -> getSentientAnimalText animal