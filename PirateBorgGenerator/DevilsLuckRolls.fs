﻿module PirateBorgGenerator.DevilsLuckRolls
open PirateBorgGenerator.Character
open PirateBorgGenerator.CharacterClassDevilsLuckAdjustments
open PirateBorgGenerator.Dice
let rollDevilsLuck character =
    let dieToRoll =
        match character.characterClass with
        | Some characterClass -> getCharacterClassDevilsLuckDie characterClass
        | None -> D2
    
    { character with
          devilsLuck = rollDie dieToRoll
          devilsLuckDie = dieToRoll }