﻿module PirateBorgGenerator.LookupCharacterClass
open PirateBorgGenerator.CharacterClasses
open PirateBorgGenerator.CharacterClassRolls
let classStrings =
    [|
        "brute";
        "rapscallion";
        "buccaneer";
        "swashbuckler";
        "zealot";
        "sorcerer";
    |]

let classStringsContain str =
    (str, classStrings) ||> Array.contains

let getCharacterClassFromString input =
    match classStringsContain input with
    | true ->
        match input with
        | "brute" -> rollForBrute |> Some
        | "rapscallion" -> rollForRapscallion |> Some
        | "buccaneer" ->  rollForBuccaneer |> Some
        | "swashbuckler" -> rollForSwashbuckler |> Some
        | "zealot" -> rollForZealot |> Some
        | "sorcerer" -> rollForSorcerer |> Some
        | _ -> None
    | false -> None