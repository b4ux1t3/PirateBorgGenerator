module PirateBorgGenerator.StartingGear

type Container =
    | Bucket
    | Bandolier
    | Satchel
    | Backpack
    | LargeSeaChest

type CharacterCreationContainer =
    | Container of Container
    | Dinghy

type Pet =
    | Snake
    | Rat
    | Lizard
    | Monkey
    | Parrot
    | Cat
    | Dog
    | Hawk
    | HermitCrab
    | FishInAJar

type CheapGear =
    | Lantern
    | Candles
    | Rope
    | Shovel
    | MedKit
    | WeightedDice
    | FlintAndSteel
    | HammerAndNails
    | MessKit
    | PipeAndTobaccoPouch
    | Torches
    | Pet of Pet

type AncientRelic =
    | CrossOfTheParagon
    | ConchShellFromTheAbyss
    | MapInkedInEctoplasm
    | WillOTheWispLantern
    | PagesFromTheNecronomicon
    | RuneEncrustedFlintlockPistol
    | JadeDie
    | UndeadBird
    | MermaidScales
    | CharonsObol
    | DupOfTheCarpenter
    | HeartOfTheSea
    | NecklaceOfEyeballs
    | CrownOfTheSunkenLord
    | CrystallineSkull
    | CodexTablet
    | SkeletonKey
    | MummifiedMonkeyHead
    | GreatOldOneFigurine
    | BrokenCompass

type Instrument =
    | Concertina
    | Drum
    | Flute
    | Fiddle
    | Banjo
    | Horn
    | HurdyGurdy
    | Guitar
    | Mandolin
    | VoiceOfAnAngel

type FancyGear =
    | Compass
    | Spyglass
    | FishingRod
    | Relic of AncientRelic
    | BottleOfFineRum
    | OldPocketWatch
    | BlanketAndPillow
    | InkQuillParchment
    | WornOutBook
    | Tent
    | Whetstone
    | Instrument of Instrument
