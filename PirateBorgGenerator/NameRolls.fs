﻿module PirateBorgGenerator.NameRolls
open PirateBorgGenerator.Dice
open PirateBorgGenerator.Character
open PirateBorgGenerator.PirateNames
let rollForFirstName character =
    let dieRoll = D12 |> rollDie
    match dieRoll with
    | 1 -> { character with name = $"%A{Edmund}" }
    | 2 -> { character with name = $"%A{Roger}" }
    | 3 -> { character with name = $"%A{Jack}" }
    | 4 -> { character with name = $"%A{David}" }
    | 5 -> { character with name = $"%A{Peter}" }
    | 6 -> { character with name = $"%A{Angelica}" }
    | 7 -> { character with name = $"%A{Samuel}" }
    | 8 -> { character with name = $"%A{Morgan}" }
    | 9 -> { character with name = $"%A{Diego}" }
    | 10 -> { character with name = $"%A{Edward}" }
    | 11 -> { character with name = $"%A{Isabella}" }
    | 12 -> { character with name = $"%A{Charles}" }
    | i -> failwith $"%i{i}"
    
let rollForSecondName character =
    let dieRoll = D12 |> rollDie
    match dieRoll with
    | 1 -> { character with name = character.name + $" %A{James}" }
    | 2 -> { character with name = character.name + $" %A{Christopher}" }
    | 3 -> { character with name = character.name + $" %A{Robert}" }
    | 4 -> { character with name = character.name + $" %A{Francois}" }
    | 5 -> { character with name = character.name + $" %A{Juan}" }
    | 6 -> { character with name = character.name + $" %A{Johnathan}" }
    | 7 -> { character with name = character.name + $" %A{Butcher}" }
    | 8 -> { character with name = character.name + $" %A{OldBen}" }
    | 9 -> { character with name = character.name + $" %A{William}" }
    | 10 -> { character with name = character.name + $" %A{Louis}" }
    | 11 -> { character with name = character.name + $" %A{Jean}" }
    | 12 -> { character with name = character.name + $" %A{Stede}" }
    | i -> failwith $"%i{i}"
    
let rollForLastName character =
    let dieRoll = D12 |> rollDie
    match dieRoll with
    | 1 -> { character with name = character.name + $" %A{Meathook}" }
    | 2 -> { character with name = character.name + $" %A{Jose}" }
    | 3 -> { character with name = character.name + $" %A{Fernando}" }
    | 4 -> { character with name = character.name + $" %A{Henry}" }
    | 5 -> { character with name = character.name + $" %A{Mary}" }
    | 6 -> { character with name = character.name + $" %A{Anne}" }
    | 7 -> { character with name = character.name + $" %A{Phillip}" }
    | 8 -> { character with name = character.name + $" %A{Scraggs}" }
    | 9 -> { character with name = character.name + $" %A{Elizabeth}" }
    | 10 -> { character with name = character.name + $" %A{Hector}" }
    | 11 -> { character with name = character.name + $" %A{Genny}" }
    | 12 -> { character with name = character.name + $" %A{Thomas}" }
    | i -> failwith $"%i{i}"