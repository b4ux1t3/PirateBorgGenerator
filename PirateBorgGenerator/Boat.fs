module PirateBorgGenerator.Boat

type Boat =
    | Dinghy
    | Sloop
