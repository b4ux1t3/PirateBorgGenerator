﻿module PirateBorgGenerator.CharacterClassWeaponAdjustments

open PirateBorgGenerator.CharacterClasses
open PirateBorgGenerator.Dice

let getClassGearDie characterClass =
    match characterClass with
    | Brute _ -> None
    | Rapscallion _ -> D6 |> Some
    | Buccaneer _ -> None
    | Swashbuckler _ -> D8 |> Some
    | Zealot _ -> D10 |> Some
    | Sorcerer _ -> D10 |> Some
    | SentientAnimal animal ->
        match animal with
        | CleverMonkey -> D10 |> Some
        | _ -> None