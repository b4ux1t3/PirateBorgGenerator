﻿module PirateBorgGenerator.CharacterClassDevilsLuckAdjustments
open PirateBorgGenerator.CharacterClasses
open PirateBorgGenerator.Dice
let getCharacterClassDevilsLuckDie characterClass =
    match characterClass with
    | Brute _ 
    | Rapscallion _ 
    | Buccaneer _ 
    | Swashbuckler _ -> D2
    | Zealot _ -> D4
    | Sorcerer _ -> D4
    | SentientAnimal animal ->
        match animal with
        | Crocodile  
        | FoulFowl 
        | Jaguar 
        | BilgeRat 
        | CleverMonkey -> D4
        | LuckyParrot -> D6
