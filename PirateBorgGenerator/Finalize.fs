module PirateBorgGenerator.Finalize

open System.Text.RegularExpressions
open PirateBorgGenerator.Character
open PirateBorgGenerator.CharacterClasses
open PirateBorgGenerator.Dice
open PirateBorgGenerator.Gear
open PirateBorgGenerator.StartingGear
open PirateBorgGenerator.StartingWeaponRolls
open PirateBorgGenerator.ClothingRolls
open PirateBorgGenerator.SpecialAbilityText

type FinalCharacter =
    { name: string
      hitPoints: int
      container: string
      cheapGear: string
      fancyGear: string
      statBlock: StatBlock
      hauntedSoul: string
      tallTale: string
      characterClass: string
      classAbility: string
      classAbilityDescription: string
      startingWeapon: string
      startingClothes: string
      clothingDescription: string
      startingHat: string
      devilsLuck: int
      devilsLuckDie: string
      weaponDie: string
      weaponDescription: string }
    
let characterToFinal (character: Character) =
    let name = character.name

    let characterClass =
        match character.characterClass with
        | Some charClass -> $"%A{charClass}"
        | None -> "Landlubber"

    let container =
        match character.container with
        | Some c ->
            match c with
            | Container cont -> $"%A{cont}"
            | Dinghy -> "Dinghy"
        | None -> ""

    let statBlock =
        match character.stats with
        | Some block -> block
        | None -> blankStatBlock

    let fancyGear =
        match character.fancyGear with
        | Some gear ->
            match gear with
            | Relic relic -> $"%A{relic}"
            | Instrument instrument -> $"%A{instrument}"
            | normalGear -> $"%A{normalGear}"
        | None -> ""

    let cheapGear =
        match character.cheapGear with
        | Some gear -> $"%A{gear}"
        | None -> ""

    let hauntedSoul =
        match character.hauntedSoul with
        | Some soul -> $"%A{soul}"
        | None -> ""

    let tallTale =
        match character.tallTale with
        | Some tale -> $"%A{tale}"
        | None -> ""

    let startingWeapon =
        match character.characterClass with
        | Some characterClass ->
            match characterClass with
            | Brute w -> $"%A{w}"
            | SentientAnimal animal -> getSentientAnimalWeapon animal
            | _ ->
                match character.startingWeapon with
                | Some weapon ->
                    match weapon with
                    | SmallSlashy w ->  $"%A{w}"
                    | LilStabby w ->  $"%A{w}"
                    | BigStabby w -> $"%A{w}"
                    | w ->  $"%A{w}"
                | None -> ""
        | None ->
            match character.startingWeapon with
            | Some weapon ->
                match weapon with
                | SmallSlashy w ->  $"%A{w}"
                | LilStabby w ->  $"%A{w}"
                | BigStabby w -> $"%A{w}"
                | w ->  $"%A{w}"
            | None -> ""
        
    let weaponDie =
        match character.characterClass with
        | Some characterClass ->
            match characterClass with
            | SentientAnimal animal ->
                animal
                |> getSentientAnimalWeaponDie
                |> getDieRollString
            | Brute weapon ->
                weapon
                |> getBruteWeaponDie
                |> getDieRollString
            | _ ->
                match character.startingWeapon with
                | Some weapon ->
                    weapon
                    |> getWeaponDamageDie
                    |> getDieRollString
                | None -> ""
        | None -> 
            match character.startingWeapon with
            | Some weapon ->
                weapon
                |> getWeaponDamageDie
                |> getDieRollString
            | None -> ""
    let weaponDescriptionString =
        match character.characterClass with
        | Some characterClass ->
            match characterClass with
            | Brute weapon -> getBruteWeaponText weapon
            | SentientAnimal animal -> getSentientAnimalWeaponDescription animal
            | _ -> 
                match character.startingWeapon with
                | Some weapon -> getWeaponDescription weapon
                | None -> ""
        | None ->
            match character.startingWeapon with
            | Some weapon -> getWeaponDescription weapon
            | None -> ""
    
    let startingClothes =
        match character.startingClothes with
        | Some clothes -> $"%A{clothes}"
        | None -> ""
        
    let clothingDescription =
        match character.startingClothes with
        | Some clothes -> getClothingDescription clothes
        | None -> ""
        
    let startingHat =
        match character.startingHat with
        | Some hat -> $"%A{hat}"
        | None -> ""

    let splitClassInfo =
        match characterClass.Split(" ") with
        | [| name; ability |] -> (name, ability)
        | _ -> ("Landlubber", "")

    let className =
        match splitClassInfo with
        | name, _ -> name

    let classAbility =
        match splitClassInfo with
        | _, ability -> ability
        
    let classAbilityText =
        match character.characterClass with
        | Some className ->
            match className with
            | Brute _ -> ""
            | characterClass ->
                getClassAbilityText characterClass
        | None -> ""

    let splitPascalCase str =
        Regex.Replace(str, @"([A-Za-z])([A-Z])", "$1 $2")

    { name = name |> splitPascalCase
      container = container |> splitPascalCase
      characterClass = className |> splitPascalCase
      classAbility = classAbility |> splitPascalCase
      classAbilityDescription =  classAbilityText
      hitPoints = character.hitPoints
      statBlock = statBlock
      fancyGear = fancyGear |> splitPascalCase
      cheapGear = cheapGear |> splitPascalCase
      hauntedSoul = hauntedSoul |> splitPascalCase
      tallTale = tallTale |> splitPascalCase
      startingWeapon = startingWeapon |> splitPascalCase
      startingClothes = startingClothes |> splitPascalCase
      clothingDescription = clothingDescription 
      startingHat = startingHat |> splitPascalCase
      devilsLuck = character.devilsLuck
      devilsLuckDie = getDieString character.devilsLuckDie
      weaponDescription = weaponDescriptionString
      weaponDie = weaponDie }
