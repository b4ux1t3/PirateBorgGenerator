﻿module PirateBorgGenerator.Character

open PirateBorgGenerator.CharacterClasses
open PirateBorgGenerator.Dice
open PirateBorgGenerator.Gear
open PirateBorgGenerator.StartingGear

type StatBlock =
    { strength: int
      agility: int
      presence: int
      toughness: int
      spirit: int }

let blankStatBlock =
    { strength = 0
      agility = 0
      presence = 0
      toughness = 0
      spirit = 0 }

let addStatBlock sb1 sb2 =
    { strength = sb1.strength + sb2.strength
      agility = sb1.agility + sb2.agility
      presence = sb1.presence + sb2.presence
      toughness = sb1.toughness + sb2.toughness
      spirit = sb1.spirit + sb2.spirit }

type Character =
    { name: string
      container: CharacterCreationContainer option
      cheapGear: CheapGear option
      fancyGear: FancyGear option
      stats: StatBlock option
      hitPoints: int
      hauntedSoul: HauntedSoul option
      tallTale: TallTale option
      characterClass: CharacterClass option
      startingWeapon: Weapon option
      startingClothes: Clothing option
      startingHat: Hat option
      devilsLuck: int
      devilsLuckDie: DieSize }

let newCharacter name =
    { name = name
      hitPoints = 0
      container = None
      cheapGear = None
      fancyGear = None
      stats = None
      hauntedSoul = None
      tallTale = None
      characterClass = None
      startingWeapon = None
      startingClothes = None
      startingHat = None
      devilsLuck = 0
      devilsLuckDie = D2 }

let addCharacterClass character characterClass =
    { character with
        characterClass = characterClass }
