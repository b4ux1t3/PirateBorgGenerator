﻿module PirateBorgGenerator.StartingWeaponRolls

open PirateBorgGenerator.CharacterClasses
open PirateBorgGenerator.Dice
open PirateBorgGenerator.Gear
open PirateBorgGenerator.CharacterClassWeaponAdjustments
open PirateBorgGenerator.Character

let rollForWeapon character =

    let rollForSmallStabby character =
        match rollDie D2 with
        | 1 ->
            { character with
                startingWeapon = Marlinspike |> LilStabby |> Some }
        | 2 ->
            { character with
                startingWeapon = BelayingPin |> LilStabby |> Some }
        | i -> failwith $"%d{i}"

    let rollForBigStabby character =
        match rollDie D2 with
        | 1 ->
            { character with
                startingWeapon = Knife |> BigStabby |> Some }
        | 2 ->
            { character with
                startingWeapon = Bayonet |> BigStabby |> Some }
        | i -> failwith $"%d{i}"

    let rollForSmallSlashy character =
        match rollDie D2 with
        | 1 ->
            { character with
                startingWeapon = SmallSword |> SmallSlashy |> Some }
        | 2 ->
            { character with
                startingWeapon = Machete |> SmallSlashy |> Some }
        | i -> failwith $"%d{i}"

    let dieToRoll =
        match character.characterClass with
        | Some characterClass -> getClassGearDie characterClass
        | None -> D10 |> Some
    
    let rollForCharacter character =
        match dieToRoll with
        | Some die ->
            match rollDie die with
            | 1 -> rollForSmallStabby character
            | 2 -> rollForBigStabby character
            | 3 -> rollForSmallSlashy character
            | 4 ->
                { character with
                    startingWeapon = CatONineTails |> Some }
            | 5 ->
                { character with
                    startingWeapon = BoardingAxe |> Some }
            | 6 ->
                { character with
                    startingWeapon = FlintlockPistol |> Some }
            | 7 ->
                { character with
                    startingWeapon = Cutlass |> Some }
            | 8 ->
                { character with
                    startingWeapon = FinelyCraftedRapier |> Some }
            | 9 ->
                { character with
                    startingWeapon = BoardingPike |> Some }
            | 10 ->
                { character with
                    startingWeapon = Musket |> Some }
            | i -> failwith $"%d{i}"
        | None -> character
        
    match character.characterClass with
    | Some characterClass ->
        match characterClass with
        | Buccaneer _ -> {character with startingWeapon = Musket |> Some }
        | _ -> rollForCharacter character
    | None -> rollForCharacter character
        
    
let getWeaponDamageDie weapon =
    match weapon with
    | LilStabby _ 
    | BigStabby _ 
    | SmallSlashy _ 
    | CatONineTails -> (1, D4) |> DieRoll
    | BoardingAxe
    | Cutlass -> (1, D6) |> DieRoll
    | FlintlockPistol -> (2, D4) |> DieRoll
    | FinelyCraftedRapier -> (1, D8) |> DieRoll
    | BoardingPike -> (1, D10) |> DieRoll
    | Musket -> (2, D6) |> DieRoll

let getWeaponDescription weapon =
    match weapon with
    | CatONineTails -> "10' reach"
    | FlintlockPistol -> "reload 2 actions, range 30', ammo: 10 + Presence rounds of shot"
    | LilStabby _ 
    | BigStabby _ 
    | SmallSlashy _ 
    | BoardingAxe
    | Cutlass 
    | FinelyCraftedRapier -> ""
    | BoardingPike -> "10' reach"
    | Musket -> "reload 2 actions, range 150', ammo: 10 + Presence rounds of shot"
    
let getSentientAnimalWeapon animal =
    match animal with
    | FoulFowl
    | LuckyParrot -> "Beak Peck"
    | Jaguar -> "Bite/Claws"
    | BilgeRat -> "Diseased bite"
    | Crocodile 
    | CleverMonkey -> "Bite"

let getSentientAnimalWeaponDescription animal =
    match animal with
    | FoulFowl
    | LuckyParrot 
    | Jaguar 
    | Crocodile 
    | CleverMonkey -> ""
    | BilgeRat -> "1-in-6 chance the target dies in d4 rounds from whatever disease you are carrying."
 
let getBruteWeaponDie weapon =
    match weapon with
    | PartOfABrokenMast
    | BrassAnchor 
    | WhalingHarpoon -> (1, D8) |> DieRoll
    | MeatCleaver -> (1, D4) |> DieRoll
    | RunicMachete -> (1, D6) |> DieRoll
    | RottenCargoNet -> (1, D2) |> DieRoll
    
let getSentientAnimalWeaponDie animal =
    match animal with
    | FoulFowl -> (1, D2) |> DieRoll
    | LuckyParrot -> (1, D4) |> DieRoll
    | Jaguar -> (1, D8) |> DieRoll
    | BilgeRat -> (1, D4) |> DieRoll
    | Crocodile -> (1, D10) |> DieRoll
    | CleverMonkey -> (1, D4) |> DieRoll