﻿module PirateBorgGenerator.StatRolls
    open PirateBorgGenerator.Dice
    open PirateBorgGenerator.Character
    open PirateBorgGenerator.CharacterClassStatAdjustments
    let collapseStats rollResult =
        match rollResult with
        | 17
        | 18 -> 3
        | 15
        | 16  -> 2
        | 13
        | 14  -> 1
        | 9
        | 10
        | 11
        | 12  -> 0
        | 7
        | 8  -> -1
        | 5
        | 6  -> -2
        | 3
        | 4  -> -3
        | _ -> 0
    let rollRandomStatBlock classAdjustments =
        let strength = (makeDieRoll 3 6) |> rollNDice |> Seq.sum |> collapseStats
        let agility = (makeDieRoll 3 6) |> rollNDice |> Seq.sum |> collapseStats
        let presence = (makeDieRoll 3 6) |> rollNDice |> Seq.sum |> collapseStats
        let toughness = (makeDieRoll 3 6) |> rollNDice |> Seq.sum |> collapseStats
        let spirit = (makeDieRoll 3 6) |> rollNDice |> Seq.sum |> collapseStats
        
        let newStatBlock =
          { strength = strength
            agility = agility
            presence = presence
            toughness = toughness
            spirit = spirit }
          
        addStatBlock newStatBlock classAdjustments
    
    
    let handleDefinedClass characterClass =
        characterClass
        |> lookupClassStatBlocks
        |> rollRandomStatBlock
        
    
    let handleLandLubber character =
        {character with stats = blankStatBlock |> rollRandomStatBlock |> Some}
    
    let rollForStats character =
        match character.characterClass with
        | Some characterClass -> {character with stats = characterClass |> handleDefinedClass |> Some}
        | None -> handleLandLubber character
    