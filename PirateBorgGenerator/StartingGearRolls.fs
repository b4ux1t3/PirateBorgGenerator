﻿module PirateBorgGenerator.StartingGearRolls

open PirateBorgGenerator.Character
open PirateBorgGenerator.Dice
open PirateBorgGenerator.StartingGear

let rollStartingContainer character =
    let dieRoll = D6 |> rollDie

    let container =
        match dieRoll with
        | 1 -> Bucket |> Container |> Some
        | 2 -> Bandolier |> Container |> Some
        | 3 -> Satchel |> Container |> Some
        | 4 -> Backpack |> Container |> Some
        | 5 -> LargeSeaChest |> Container |> Some
        | 6 -> Dinghy |> Some
        | _ -> None

    { character with container = container }

let rollForPet () =
    let dieRoll = D10 |> rollDie

    match dieRoll with
    | 1 -> Snake
    | 2 -> Rat
    | 3 -> Lizard
    | 4 -> Monkey
    | 5 -> Parrot
    | 6 -> Cat
    | 7 -> Dog
    | 8 -> Hawk
    | 9 -> HermitCrab
    | 10 -> FishInAJar
    | i -> failwith $"%d{i}"

let rollCheapGear character =
    let dieRoll = D12 |> rollDie

    let cheapGear =
        match dieRoll with
        | 1 -> Lantern |> Some
        | 2 -> Candles |> Some
        | 3 -> Rope |> Some
        | 4 -> Shovel |> Some
        | 5 -> MedKit |> Some
        | 6 -> WeightedDice |> Some
        | 7 -> FlintAndSteel |> Some
        | 8 -> HammerAndNails |> Some
        | 9 -> MessKit |> Some
        | 10 -> PipeAndTobaccoPouch |> Some
        | 11 -> Torches |> Some
        | 12 -> rollForPet () |> Pet |> Some
        | _ -> None

    { character with cheapGear = cheapGear }

let rollForAncientRelic () =
    let dieRoll = D20 |> rollDie

    match dieRoll with
    | 1 -> CrossOfTheParagon
    | 2 -> ConchShellFromTheAbyss
    | 3 -> MapInkedInEctoplasm
    | 4 -> WillOTheWispLantern
    | 5 -> PagesFromTheNecronomicon
    | 6 -> RuneEncrustedFlintlockPistol
    | 7 -> JadeDie
    | 8 -> UndeadBird
    | 9 -> MermaidScales
    | 10 -> CharonsObol
    | 11 -> DupOfTheCarpenter
    | 12 -> HeartOfTheSea
    | 13 -> NecklaceOfEyeballs
    | 14 -> CrownOfTheSunkenLord
    | 15 -> CrystallineSkull
    | 16 -> CodexTablet
    | 17 -> SkeletonKey
    | 18 -> MummifiedMonkeyHead
    | 19 -> GreatOldOneFigurine
    | 20 -> BrokenCompass
    | i -> failwith $"%d{i}"

let rollForInstrument () =
    let dieRoll = D10 |> rollDie

    match dieRoll with
    | 1 -> Concertina
    | 2 -> Drum
    | 3 -> Flute
    | 4 -> Fiddle
    | 5 -> Banjo
    | 6 -> Horn
    | 7 -> HurdyGurdy
    | 8 -> Guitar
    | 9 -> Mandolin
    | 10 -> VoiceOfAnAngel
    | i -> failwith $"%d{i}"

let rollFancyGear character =
    let dieRoll = D12 |> rollDie

    let fancyGear =
        match dieRoll with
        | 1 -> Compass |> Some
        | 2 -> Spyglass |> Some
        | 3 -> FishingRod |> Some
        | 4 -> rollForAncientRelic () |> Relic |> Some
        | 5 -> BottleOfFineRum |> Some
        | 6 -> OldPocketWatch |> Some
        | 7 -> BlanketAndPillow |> Some
        | 8 -> InkQuillParchment |> Some
        | 9 -> WornOutBook |> Some
        | 10 -> Tent |> Some
        | 11 -> Whetstone |> Some
        | 12 -> rollForInstrument () |> Instrument |> Some
        | _ -> None

    { character with fancyGear = fancyGear }
