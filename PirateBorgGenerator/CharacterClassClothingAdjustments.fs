﻿module PirateBorgGenerator.CharacterClassClothingAdjustments

open PirateBorgGenerator.CharacterClasses
open PirateBorgGenerator.Dice

let getCharacterClassClothingDie characterClass =
    match characterClass with
    | Brute _ -> D10 |> Some
    | Rapscallion _ -> D6 |> Some
    | Buccaneer _ -> D10 |> Some
    | Swashbuckler _ -> D10 |> Some
    | Zealot _ -> D8 |> Some
    | Sorcerer _ -> D6 |> Some
    | SentientAnimal _ -> None

let getCharacterClassHatDie characterClass =
    match characterClass with
    | Rapscallion _ -> D10 |> Some
    | Brute _
    | Buccaneer _
    | Swashbuckler _ -> D12 |> Some
    | Zealot _ 
    | Sorcerer _ 
    | SentientAnimal _ -> None