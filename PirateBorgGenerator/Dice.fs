module PirateBorgGenerator.Dice

open System

type DieSize = DieSize of int
type DieRoll = DieRoll of int * DieSize

let makeDieRoll n dieSize = (n, dieSize |> DieSize) |> DieRoll

let rollDie die =
    match die with
    | DieSize n -> 1 + Random.Shared.Next(n)

let rollDieWithCount die n = (n, rollDie die)

let extractRoll roll =
    match roll with
    | _, roll -> roll

let rollNDice dieRoll =
    match dieRoll with
    | DieRoll(n, die) ->
        let rollThisDie = rollDieWithCount die
        seq { 1..n } |> Seq.map rollThisDie |> Seq.map extractRoll

let D2 = 2 |> DieSize
let D4 = 4 |> DieSize
let D6 = 6 |> DieSize
let D8 = 8 |> DieSize
let D10 = 10 |> DieSize
let D12 = 12 |> DieSize
let D20 = 20 |> DieSize

let getDieString die =
    match die with
    | DieSize n -> $"d%d{n}"
        

let rec getDieRollString dieRoll =
    match dieRoll with
    | DieRoll (number, die) when number > 1 -> $"%d{number}%s{getDieString die}"
    | DieRoll (_, die) -> $"%s{getDieString die}"