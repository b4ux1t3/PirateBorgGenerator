# PirateBorgGenerator - The Library

This .NET F# project contains all of the character generation logic. This includes:

- Classes
- Dice rolls
- Starting gear
- Stats
- Pretty much anything else that is referenced in the Character creation section of the rulebook.
  
All of itsmembers are publically accessible, but it is more-or--less meant to be interacted with through the interface in [`Generation.fs`](./Generation.fs), specifically the `generate` function.
