﻿module PirateBorgGenerator.CharacterClasses

type BruteWeapon =
    | BrassAnchor
    | WhalingHarpoon
    | MeatCleaver
    | PartOfABrokenMast
    | RunicMachete
    | RottenCargoNet

type RapscallionProfession =
    | BackStabber
    | Burglar
    | RopeMonkey
    | SneakyBastard
    | LuckyDevil
    | GrogBrewer

type BuccaneerFeature =
    | TreasureHunter
    | CrackShot
    | FixBayonets
    | FocusedAim
    | BuccanCook
    | Survivalist

type SwashbucklerStyle =
    | OstentatiousFencer
    | FlintlockFanatic
    | ScurvyScallywag
    | InspiringLeader
    | KnifeKnave
    | BlackPowderPoet

type ZealotPrayer =
    | Heal
    | Curse
    | DeathWard
    | ControlWeather
    | BlessedGuidance
    | HolyProtection
    | DivineLight
    | Silence
    | Sanctuary
    | Commune

type SorcererSpell =
    | DeadHead
    | SpiritualPossession
    | Protection
    | Clairvoyance
    | NecroSleep
    | RaiseTheDead

type HauntedSoul =
    | Ghost
    | Conduit
    | EldritchMind
    | Zombie
    | Vampirism
    | Skeleton

type SentientAnimal =
    | FoulFowl
    | Jaguar
    | Crocodile
    | BilgeRat
    | LuckyParrot
    | CleverMonkey


type AquaticMutantHeritage =
    | Anglerfish
    | Crab
    | Jellyfish
    | Octopus
    | SeaTurtle
    | ElectricEel
    | Shark
    | TheGreatOldOne //TODO: Implement rituals for this.

type TallTale =
    | Merfolk
    | AquaticMutant of AquaticMutantHeritage
    | SentientAnimal of SentientAnimal

type CharacterClass =
    | Brute of BruteWeapon
    | Rapscallion of RapscallionProfession
    | Buccaneer of BuccaneerFeature
    | Swashbuckler of SwashbucklerStyle
    | Zealot of ZealotPrayer
    | Sorcerer of SorcererSpell
    | SentientAnimal of SentientAnimal
