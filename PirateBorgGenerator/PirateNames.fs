﻿module PirateBorgGenerator.PirateNames

type FirstNames =
    | Edmund
    | Roger
    | Jack
    | David
    | Peter
    | Angelica
    | Samuel
    | Morgan
    | Diego
    | Edward
    | Isabella
    | Charles

type MiddleNames =
    | James
    | Christopher
    | Robert
    | Francois
    | Juan
    | Johnathan
    | Butcher
    | OldBen
    | William
    | Louis
    | Jean
    | Stede
    
type LastNames =
    | Meathook
    | Jose
    | Fernando
    | Henry
    | Mary
    | Anne
    | Phillip
    | Scraggs
    | Elizabeth
    | Hector
    | Genny
    | Thomas