﻿module PirateBorgGenerator.Gear

type LilStabby =
    | Marlinspike
    | BelayingPin

type BigStabby =
    | Knife
    | Bayonet

type SmallSlashy =
    | SmallSword
    | Machete

type Weapon =
    | LilStabby of LilStabby
    | BigStabby of BigStabby
    | SmallSlashy of SmallSlashy
    | CatONineTails
    | BoardingAxe
    | FlintlockPistol
    | Cutlass
    | FinelyCraftedRapier
    | BoardingPike
    | Musket

type Clothing =
    | Rags
    | CommonClothes
    | OldUniform
    | FancyClothes
    | LeatherArmor
    | HideArmor
    | ChainShirt
    | ConquistadorPlate

type Hat =
    | Wig
    | Bandanna
    | Cavalier
    | Bicorne
    | PlainTricorne
    | FancyTricorne
    | MetalLinedHat
    | Morion
