﻿module PirateBorgGenerator.CharacterClassRolls

open PirateBorgGenerator.CharacterClasses
open PirateBorgGenerator.Dice
open PirateBorgGenerator.Character

let rollForBrute character =
    let dieRoll = D6 |> rollDie

    match dieRoll with
    | 1 ->
        { character with
            characterClass = BrassAnchor |> Brute |> Some }
    | 2 ->
        { character with
            characterClass = WhalingHarpoon |> Brute |> Some }
    | 3 ->
        { character with
            characterClass = MeatCleaver |> Brute |> Some }
    | 4 ->
        { character with
            characterClass = PartOfABrokenMast |> Brute |> Some }
    | 5 ->
        { character with
            characterClass = RunicMachete |> Brute |> Some }
    | 6 ->
        { character with
            characterClass = RottenCargoNet |> Brute |> Some }
    | i ->
        printfn $"%d{i}"
        failwith $"%d{i}"

let rollForRapscallion character =
    let dieRoll = D6 |> rollDie

    match dieRoll with
    | 1 ->
        { character with
            characterClass = BackStabber |> Rapscallion |> Some }
    | 2 ->
        { character with
            characterClass = Burglar |> Rapscallion |> Some }
    | 3 ->
        { character with
            characterClass = RopeMonkey |> Rapscallion |> Some }
    | 4 ->
        { character with
            characterClass = SneakyBastard |> Rapscallion |> Some }
    | 5 ->
        { character with
            characterClass = LuckyDevil |> Rapscallion |> Some }
    | 6 ->
        { character with
            characterClass = GrogBrewer |> Rapscallion |> Some }
    | i ->
        printfn $"%d{i}"
        failwith $"%d{i}"

let rollForBuccaneer character =
    let dieRoll = D6 |> rollDie

    match dieRoll with
    | 1 ->
        { character with
            characterClass = TreasureHunter |> Buccaneer |> Some }
    | 2 ->
        { character with
            characterClass = CrackShot |> Buccaneer |> Some }
    | 3 ->
        { character with
            characterClass = FixBayonets |> Buccaneer |> Some }
    | 4 ->
        { character with
            characterClass = FocusedAim |> Buccaneer |> Some }
    | 5 ->
        { character with
            characterClass = BuccanCook |> Buccaneer |> Some }
    | 6 ->
        let newHitPoints = character.hitPoints + (rollDie D4)
        
        { character with
            characterClass = Survivalist |> Buccaneer |> Some
            hitPoints = newHitPoints
             }
    | i ->
        printfn $"%d{i}"
        failwith $"%d{i}"

let rollForSorcerer character =
    let dieRoll = D6 |> rollDie

    match dieRoll with
    | 1 ->
        { character with
            characterClass = OstentatiousFencer |> Swashbuckler |> Some }
    | 2 ->
        { character with
            characterClass = FlintlockFanatic |> Swashbuckler |> Some }
    | 3 ->
        { character with
            characterClass = ScurvyScallywag |> Swashbuckler |> Some }
    | 4 ->
        { character with
            characterClass = InspiringLeader |> Swashbuckler |> Some }
    | 5 ->
        { character with
            characterClass = KnifeKnave |> Swashbuckler |> Some }
    | 6 ->
        { character with
            characterClass = BlackPowderPoet |> Swashbuckler |> Some }
    | i ->
        printfn $"%d{i}"
        failwith $"%d{i}"

let rollForZealot character =
    let dieRoll = D6 |> rollDie

    match dieRoll with
    | 1 ->
        { character with
            characterClass = Heal |> Zealot |> Some }
    | 2 ->
        { character with
            characterClass = Curse |> Zealot |> Some }
    | 3 ->
        { character with
            characterClass = DeathWard |> Zealot |> Some }
    | 4 ->
        { character with
            characterClass = ControlWeather |> Zealot |> Some }
    | 5 ->
        { character with
            characterClass = BlessedGuidance |> Zealot |> Some }
    | 6 ->
        { character with
            characterClass = HolyProtection |> Zealot |> Some }
    | 7 ->
        { character with
            characterClass = DivineLight |> Zealot |> Some }
    | 8 ->
        { character with
            characterClass = Silence |> Zealot |> Some }
    | 9 ->
        { character with
            characterClass = Sanctuary |> Zealot |> Some }
    | 10 ->
        { character with
            characterClass = Commune |> Zealot |> Some }
    | i ->
        printfn $"%d{i}"
        failwith $"%d{i}"

let rollForSwashbuckler character =
    let dieRoll = D6 |> rollDie

    match dieRoll with
    | 1 ->
        { character with
            characterClass = DeadHead |> Sorcerer |> Some }
    | 2 ->
        { character with
            characterClass = SpiritualPossession |> Sorcerer |> Some }
    | 3 ->
        { character with
            characterClass = Protection |> Sorcerer |> Some }
    | 4 ->
        { character with
            characterClass = Clairvoyance |> Sorcerer |> Some }
    | 5 ->
        { character with
            characterClass = NecroSleep |> Sorcerer |> Some }
    | 6 ->
        { character with
            characterClass = RaiseTheDead |> Sorcerer |> Some }
    | i ->
        printfn $"%d{i}"
        failwith $"%d{i}"

let rollForHauntedSoul character =
    let dieRoll = D6 |> rollDie

    match dieRoll with
    | 1 ->
        { character with
            hauntedSoul = Ghost |> Some }
    | 2 ->
        { character with
            hauntedSoul = Conduit |> Some }
    | 3 ->
        { character with
            hauntedSoul = EldritchMind |> Some }
    | 4 ->
        { character with
            hauntedSoul = Zombie |> Some }
    | 5 ->
        { character with
            hauntedSoul = Vampirism |> Some }
    | 6 ->
        { character with
            hauntedSoul = Skeleton |> Some }
    | i ->
        printfn $"%d{i}"
        failwith $"%d{i}"

let rollForAquaticMutant character =
    let dieRoll = D8 |> rollDie

    match dieRoll with
    | 1 ->
        { character with
            tallTale = Anglerfish |> AquaticMutant |> Some }
    | 2 ->
        { character with
            tallTale = Crab |> AquaticMutant |> Some }
    | 3 ->
        { character with
            tallTale = Jellyfish |> AquaticMutant |> Some }
    | 4 ->
        { character with
            tallTale = Octopus |> AquaticMutant |> Some }
    | 5 ->
        { character with
            tallTale = SeaTurtle |> AquaticMutant |> Some }
    | 6 ->
        { character with
            tallTale = ElectricEel |> AquaticMutant |> Some }
    | 7 ->
        { character with
            tallTale = Shark |> AquaticMutant |> Some }
    | 8 ->
        { character with
            tallTale = TheGreatOldOne |> AquaticMutant |> Some }
    | i ->
        printfn $"%d{i}"
        failwith $"%d{i}"

let rollForSentientAnimal character =
    let dieRoll = D6 |> rollDie

    match dieRoll with
    | 1 ->
        { character with
            characterClass = FoulFowl |> SentientAnimal |> Some }
    | 2 ->
        { character with
            characterClass = FoulFowl |> SentientAnimal |> Some }
    | 3 ->
        { character with
            characterClass = FoulFowl |> SentientAnimal |> Some }
    | 4 ->
        { character with
            characterClass = FoulFowl |> SentientAnimal |> Some }
    | 5 ->
        { character with
            characterClass = FoulFowl |> SentientAnimal |> Some }
    | 6 ->
        { character with
            characterClass = FoulFowl |> SentientAnimal |> Some }
    | i ->
        printfn $"%d{i}"
        failwith $"%d{i}"

let rollForTallTale character =
    let dieRoll = D6 |> rollDie

    match dieRoll with
    | 1
    | 2 ->
        { character with
            tallTale = Merfolk |> Some }
    | 3
    | 4 -> rollForAquaticMutant character
    | 5
    | 6 -> rollForSentientAnimal character
    | i ->
        printfn $"%d{i}"
        failwith $"%d{i}"

let rec rollForClass optionalClasses character =
    let dieRoll =
        match optionalClasses with
        | true -> D8 |> rollDie
        | false -> D6 |> rollDie

    match dieRoll with
    | 1 -> character |> rollForBrute
    | 2 -> character |> rollForRapscallion
    | 3 -> character |> rollForBuccaneer
    | 4 -> character |> rollForSwashbuckler
    | 5 -> character |> rollForZealot
    | 6 -> character |> rollForSorcerer
    | 7 -> (false, character) ||> rollForClass |> rollForHauntedSoul
    | 8 -> (false, character) ||> rollForClass |> rollForTallTale
    | _ -> character
