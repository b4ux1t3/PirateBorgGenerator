module PirateBorgGenerator.HealthPointsRolls

open PirateBorgGenerator.Character
open PirateBorgGenerator.CharacterClasses
open PirateBorgGenerator.Dice

let rollHpForClass characterClass toughness =
    match characterClass with
    | Rapscallion _
    | Buccaneer _
    | Zealot _
    | Sorcerer _ -> (D8 |> rollDie) + toughness
    | Swashbuckler _ -> (D10 |> rollDie) + toughness
    | Brute _ -> (D12 |> rollDie) + toughness
    | SentientAnimal sentientAnimal ->
        match sentientAnimal with
        | FoulFowl -> (D4 |> rollDie) + toughness
        | Jaguar -> (D8 |> rollDie) + toughness
        | Crocodile -> (D10 |> rollDie) + toughness
        | BilgeRat
        | LuckyParrot -> (D2 |> rollDie)
        | CleverMonkey -> (D6 |> rollDie) + toughness

let rollForHp character =
    let toughness =
        match character.stats with
        | Some n -> n.toughness
        | None -> 0

    let hp =
        match character.characterClass with
        | Some characterClass -> rollHpForClass characterClass toughness
        | None -> (D10 |> rollDie) + toughness

    { character with hitPoints = max hp 1 }
