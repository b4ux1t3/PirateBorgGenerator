﻿module PirateBorgGenerator.CharacterClassStatAdjustments

open PirateBorgGenerator.Character
open PirateBorgGenerator.CharacterClasses

let lookupClassStatBlocks characterClass =
    match characterClass with
    | Brute _ ->
        { blankStatBlock with
            strength = 1
            toughness = 1
            presence = -1
            spirit = -1 }
    | Rapscallion _ ->
        { blankStatBlock with
            agility = 2
            strength = -1
            toughness = -1 }
    | Buccaneer feature ->
        match feature with
        | Survivalist ->
            { blankStatBlock with
                agility = 2
                strength = -1
                toughness = 0 }
         | _ ->
            { blankStatBlock with
                agility = 2
                strength = -1
                toughness = -1 }
    | Swashbuckler _ ->
        { blankStatBlock with
            agility = 1
            strength = 1
            presence = -1
            spirit = -1 }
    | Zealot _ ->
        { blankStatBlock with
            agility = -1
            toughness = -1
            spirit = 2 }
    | Sorcerer _ ->
        { blankStatBlock with
            strength = -1
            toughness = -1
            spirit = 2 }
    | SentientAnimal animal ->
        match animal with
        | FoulFowl ->
            { spirit = 3
              agility = -2
              presence = -2
              strength = -2
              toughness = -2 }
        | Jaguar ->
            { strength = 2
              agility = 2
              presence = -2
              spirit = -2
              toughness = -2 }
        | Crocodile ->
            { strength = 3
              toughness = 1
              agility = -2
              presence = -2
              spirit = -2 }
        | BilgeRat ->
            { agility = 3
              toughness = 2
              presence = -2
              spirit = -2
              strength = -2 }
        | LuckyParrot ->
            { strength = 1
              presence = 2
              agility = -2
              spirit = -2
              toughness = -2 }
        | CleverMonkey ->
            { strength = -1
              agility = 2
              presence = -2
              spirit = -2
              toughness = -2 }
