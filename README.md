# PirateBorgGenerator

This project is a generator for [Pirate Borg](https://www.limithron.com/pirateborg) characters.

This project is not associated with Limithron in any way shape or form, nor is it endorsed by them. It is purely a derivative work.

This project has three parts:

1. A .NET library for generating random Pirate Borg Characters. This is located in `PirateBorgGenerator/`
2. A command-line utility exposing that functionality. This is located in `PirateBorgGenerator.Cli/`
3. A web API and page for exposing the aforementioned library. This is located in `PirateBorgGenerator.Api`

## Contributing

Contributions are always welcomed. Please open an issue against this repository first, before opening a new merge request.

Good contributions include, but are not limited to:

- Spelling corrections
- Generation logic fixes

As of right now, I'm not accepting any contributions for custom content for Pirate Borg. I am looking into ways to accept arbitrary custom content, but the generation mechanics and data are currently baked in to the F# code, so any additional content would necessarily require a significant amount of refactoring.
